# cemca-ph2-exps

DSA experiments phase 2 (CEMCA)

# Introduction

This repository holds the source codes for the following Data Structures and Algorithms labs

1. Graph Traversals
2. Minimum Spanning Trees
3. Single Source Shortest Path

All the above experiments were developed with support from CEMCA.

# Building

1. Navigate to the directories based on the directory names given in following table

   | Lab                   | Source Directory |
   | --------------------- | ---------------- |
   | Graph Traversal       | graph traversal  |
   | Minimum Spanning Tree | mst              |
   | Single Shortest Path  | shortest path    |

2. Follow the instructions given in README.md inside each directory to build and run that particular lab.
