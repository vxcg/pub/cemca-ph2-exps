import cytoscape from 'cytoscape';
import dagre from 'cytoscape-dagre';

import * as styles from "./styles.js";
import * as DG from "./dataGenerators.js";
import * as updates from "./updates.js";
import * as prompt from "./prompt.js";

cytoscape.use( dagre );

const promptId = "prompt";
prompt.update(promptId, "Select any edge to add it to the spanning tree, or remove it from the tree.",prompt.alertType.Info);

const random_nodeids = DG.randomNodeIds();


var graph = cytoscape({
    container: document.getElementById('graph'),
    autoungrabify: true,
    autounselectify: true,
    elements: {
	nodes: DG.randomNodes(random_nodeids),
	edges: DG.randomEdges(random_nodeids)
    },
    
    style: styles.graph_style,
    
    layout: {
	name: 'cose'
    }
});


var tree_cy = cytoscape({
    container: document.getElementById('tree'),
    elements: {
	nodes: [],
	edges: []
    },
    
    style: styles.tree_style,
    
    layout: {
	name: 'dagre'
    }
});


graph.on('tap', 'edge', (e) => {
    
    const edge = e.target;
    
    
    if(!isCyclic(graph)){
        prompt.update(promptId,"Select any edge to add it to the spanning tree, or remove it from the tree.",prompt.alertType.Info)
        if( edge.classes().includes('picked') ){
        /* edge is already picked.  we unpick it. */
        updates.deselectEdge(tree_cy, edge);
        }
        else{
            if(isCyclic(graph,edge)){
                /* if a cycle is formed after adding current edge, mark that edge cyclic */
                edge.addClass("picked");
                edge.addClass("cyclic");
                prompt.update(promptId,"Remove the cycle forming edge, to continue constructing Spanning Tree",prompt.alertType.Danger)
            }
            else
            {
            updates.selectEdge(tree_cy, edge);
            }
        }
    }
    else {
        /* if cycle is formed, block all interaction other than removing that cyclic edge */
        if( edge.classes().includes('cyclic') ){
            /* Cyclic edge is already picked.  we unpick it. */
            prompt.update(promptId,"Select any edge to add it to the spanning tree, or remove it from the tree.",prompt.alertType.Info)
            updates.deselectEdge(tree_cy, edge);
            }
    }
    if( isSpanning(graph, tree_cy) ){
        prompt.update(promptId, "Spanning tree complete.",prompt.alertType.Success);
        }

})





function isSpanning(g, t) {
    /*
      Check if the tree is a spanning tree for the graph.
    */
    const tNodes = t.nodes();
    const gNodes = g.nodes();
    const tEdges = t.edges();
    const allNodesSelected = tNodes.length == gNodes.length ? true : false ;
    const edgeConstraintSat = (tNodes.length - 1) == tEdges.length ? true : false;
    if (allNodesSelected && edgeConstraintSat && (!isCyclic(graph))){
        return true;
    }
    return false;
}



function isCyclic(graph_cy,edge){
    const testGraph = cytoscape({
            elements: {
            nodes: [],
            edges: []
            }
        });
    const reqNodes = graph_cy.nodes().filter(n=>n.hasClass('selected')).clone();
    const reqEdges = graph_cy.edges().filter(e=>{return (e.hasClass('picked')||e.hasClass('cyclic'));}).clone();
    testGraph.add(reqNodes);
    testGraph.add(reqEdges);
    if(edge!=undefined){
        testGraph.add(edge.source());
        testGraph.add(edge.target());
        testGraph.add(edge);
    }
    var cyclic = false;
    testGraph.nodes().forEach(r=>{
    var visited = [];
    testGraph.elements().dfs({
        root: r
        ,visit: function(v, e, u, i, depth){
            visited.push(v.id());
            const adjNodes = adjacentNodes(v);
            const parent = u!=undefined?u.id():-1;
            adjNodes.forEach(n => {
                if (visited.includes(n) && n!=parent) {
                    cyclic = true;
                }
            })          
        },
        directed: false
      
    });
    });
    testGraph.destroy();
    return cyclic;

}

function adjacentNodes(node){
    const cEdges = node.connectedEdges();
    const sources = cEdges.sources().map(n=>n.id());
    const targets = cEdges.targets().map(n=>n.id());
    var adjacent = sources.concat(targets).filter(function(v,i,self) {return i == self.indexOf(v);});
    var reqAdjacent = adjacent.filter(function(v,i,self){return v!=node.id();});
    return reqAdjacent;

}