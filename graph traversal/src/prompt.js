export function update(id, msg, alertType) {
    console.log(id, msg);
    document.querySelector(`#${id}`).className="flex justify-center shadow alert "+ alertType;
    const node = document.querySelector(`#${id} p`);
    node.innerText = `${msg}`;

}

export const alertType =    { Info: "alert-info"
                            , Danger: "alert-danger"
                            , Success: "alert-success"
                            } 
