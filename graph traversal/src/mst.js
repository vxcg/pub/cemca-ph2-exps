import cytoscape from "cytoscape";
import dagre from "cytoscape-dagre";

import * as styles from "./styles.js";
import * as DG from "./dataGenerators.js";
import * as updates from "./updates.js";
import * as prompt from "./prompt.js";

cytoscape.use(dagre);

const promptId = "prompt";
prompt.update(
  promptId,
  "Select any edge to add it to the Minimum spanning tree, or remove it from the tree.",
  prompt.alertType.Info
);

const random_nodeids = DG.randomNodeIds();

var graph = cytoscape({
  container: document.getElementById("graph"),
  // autoungrabify: true,
  autounselectify: true,
  elements: {
    nodes: DG.randomNodes(random_nodeids),
    edges: DG.randomEdgesWithWeights(random_nodeids),
  },

  style: styles.graph_style_edge_labels,

  layout: {
    name: "cose",
  },
});

var tree_cy = cytoscape({
  container: document.getElementById("tree"),
  elements: {
    nodes: [],
    edges: [],
  },

  style: styles.tree_style_edge_labels,

  layout: {
    name: "dagre",
  },
});

graph.on("tap", "edge", (e) => {
  const edge = e.target;

  if (!isCyclic(graph)) {
    prompt.update(
      promptId,
      "Select any edge to add it to the Minimum spanning tree, or remove it from the tree.",
      prompt.alertType.Info
    );
    if (edge.classes().includes("picked")) {
      /* edge is already picked.  we unpick it. */
      updates.deselectEdge(tree_cy, edge);
    } else {
      if (isCyclic(graph, edge)) {
        /* if a cycle is formed after adding current edge, mark that edge cyclic */
        edge.addClass("picked");
        edge.addClass("cyclic");
        prompt.update(
          promptId,
          "Remove the cycle forming edge, to continue constructing Minimum Spanning Tree",
          prompt.alertType.Danger
        );
      } else {
        updates.selectEdge(tree_cy, edge);
        if (!isMinimumEdge(edge)) {
          console.log(false);
          prompt.update(
            promptId,
            "Selected edge may be an incorrect edge",
            prompt.alertType.Danger
          );
        } else {
          console.log(true);
          prompt.update(
            promptId,
            "Select any edge to add it to the Minimum spanning tree, or remove it from the tree.",
            prompt.alertType.Info
          );
        }
      }
    }
  } else {
    /* if cycle is formed, block all interaction other than removing that cyclic edge */
    if (edge.classes().includes("cyclic")) {
      /* Cyclic edge is already picked.  we unpick it. */
      prompt.update(
        promptId,
        "Select any edge to add it to the Minimum spanning tree, or remove it from the tree.",
        prompt.alertType.Info
      );
      updates.deselectEdge(tree_cy, edge);
    }
  }
  if (isSpanning(graph, tree_cy)) {
    prompt.update(
      promptId,
      "Minimum Spanning tree complete.",
      prompt.alertType.Success
    );
  }
});

function isSpanning(g, t) {
  /*
      Check if the tree is a spanning tree for the graph.
    */
  const tNodes = t.nodes();
  const gNodes = g.nodes();
  const tEdges = t.edges();
  const allNodesSelected = tNodes.length == gNodes.length ? true : false;
  const edgeConstraintSat = tNodes.length - 1 == tEdges.length ? true : false;
  if (allNodesSelected && edgeConstraintSat && !isCyclic(graph)) {
    return isMST(g, t);
  }
  return false;
}

function isCyclic(graph_cy, edge) {
  const testGraph = cytoscape({
    elements: {
      nodes: [],
      edges: [],
    },
  });
  const reqNodes = graph_cy
    .nodes()
    .filter((n) => n.hasClass("selected"))
    .clone();
  const reqEdges = graph_cy
    .edges()
    .filter((e) => {
      return e.hasClass("picked") || e.hasClass("cyclic");
    })
    .clone();
  testGraph.add(reqNodes);
  testGraph.add(reqEdges);
  if (edge != undefined) {
    testGraph.add(edge.source());
    testGraph.add(edge.target());
    testGraph.add(edge);
  }
  var cyclic = false;
  testGraph.nodes().forEach((r) => {
    var visited = [];
    testGraph.elements().dfs({
      root: r,
      visit: function (v, e, u, i, depth) {
        visited.push(v.id());
        const adjNodes = adjacentNodes(v);
        const parent = u != undefined ? u.id() : -1;
        adjNodes.forEach((n) => {
          if (visited.includes(n) && n != parent) {
            cyclic = true;
          }
        });
      },
      directed: false,
    });
  });
  testGraph.destroy();
  return cyclic;
}

function adjacentNodes(node) {
  const cEdges = node.connectedEdges();
  const sources = cEdges.sources().map((n) => n.id());
  const targets = cEdges.targets().map((n) => n.id());
  var adjacent = sources.concat(targets).filter(function (v, i, self) {
    return i == self.indexOf(v);
  });
  var reqAdjacent = adjacent.filter(function (v, i, self) {
    return v != node.id();
  });
  return reqAdjacent;
}

function isMinimumEdge(edge) {
  const src = edge.source();
  const tgt = edge.target();
  const w = edge.data("weight");
  console.log(w);
  var min_src = true;
  var min_tgt = true;
  var src_edges = graph
    .nodes()
    .edgesWith(src)
    .filter((e) => {
      return e.data("weight") < w;
    });
  var tgt_edges = graph
    .nodes()
    .edgesWith(tgt)
    .filter((e) => {
      return e.data("weight") < w;
    });
  // console.log(src_edges, tgt_edges);
  src_edges.forEach((e) => {
    console.log(e.source(), e.target(), e.data("weight"));
    if (
      !e.target().classes().includes("selected") ||
      !e.source().classes().includes("selected")
    ) {
      min_src = false;
    }
  });
  tgt_edges.forEach((e) => {
    console.log(e.source(), e.target(), e.data("weight"));
    if (
      !e.source().classes().includes("selected") ||
      !e.target().classes().includes("selected")
    ) {
      min_tgt = false;
    }
  });
  return min_src || min_tgt;
}

function isMST(g, t) {
  var tw_g = 0,
    tw_t = 0;
  g.elements()
    .kruskal((ed) => {
      return ed.data("weight");
    })
    .edges()
    .forEach((e) => {
      tw_g += parseInt(e.data("weight"));
    });
  t.edges().forEach((e) => {
    tw_t += parseInt(e.data("weight"));
  });
  console.log(tw_g, tw_t);
  return tw_g == tw_t;
}
