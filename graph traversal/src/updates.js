export function deselectEdge(tree_cy, edge) {
    edge.classes('');

    const id = edge.id();
    const src = edge.source();
    const trgt = edge.target();

    
    /* remove the nodes and edges from the spanning tree. */
    tree_cy.remove('edge[id = "' + id + '"]');
    const n1 = tree_cy.elements('node[id = "' + src.id() + '"]');
    const n2 = tree_cy.elements('node[id = "' + trgt.id() + '"]');
    if (n1.degree() == 0) {
	n1.remove();
	src.removeClass('selected');
    }
    if (n2.degree() == 0) {
	n2.remove();
	trgt.removeClass('selected');
    }
    tree_cy.center();
}


export function selectEdge(tree_cy, edge) {
    
    const id = edge.id();
    const src = edge.source();
    const trgt = edge.target();

    edge.classes('picked');
    src.addClass('selected');
    trgt.addClass('selected');
    
    /* add the nodes and edges to the spanning tree graph */
    tree_cy.add(src);
    tree_cy.add(trgt);
    tree_cy.add(edge);

    tree_cy.center();
    tree_cy.zoom(2);

}


export function selectNode(tree, node, edge) {

    /*
      Given a node and an edge, add the node to the tree.
     */
    
    
}
