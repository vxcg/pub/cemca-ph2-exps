import _ from 'underscore';

/* Generate a random set of node ids 
   
   Returns a list of node ids.

   - The size of the list is in range [4, 8]
   - The range of values is in range [1, 10]

   We can parametrize these values if needed.
*/
export function randomNodeIds() {
    return _.sample(_.range(1, 10), _.sample(_.range(5, 10)));
}

export function randomNodes(nids) {
    return _.map(nids, (nid) => {
	return {
	    group: "nodes",
	    data: {
		id: String(nid)
	    }
	};
    });
}

export function randomEdges(nodeids) {
    
    const st_chain_pairs = _.rest(_.zip(nodeids,
				  _.union(_.rest(nodeids),
					  [_.first(nodeids)])));   

    const more_pairs = _.map(_.range(nodeids.length),
			     (i) => _.sample(nodeids, 2).sort());

    const st_pairs = _.uniq(_.map(_.union(st_chain_pairs, more_pairs),
				  (p) => p.sort()));
    
    return _.map(st_pairs, (stp) => {
	const [sid, tid] = stp;
	return {
	    group: "edges",
	    data: {
		id: String(sid) + String(tid),
		source: String(sid),
		target: String(tid)
	    }
	};
    });
}

export function randomEdgesWithWeights(nodeids) {
    
    const st_chain_pairs = _.rest(_.zip(nodeids,
				  _.union(_.rest(nodeids),
					  [_.first(nodeids)])));   

    const more_pairs = _.map(_.range(nodeids.length),
			     (i) => _.sample(nodeids, 2).sort());

    const st_pairs = _.uniq(_.map(_.union(st_chain_pairs, more_pairs),
				  (p) => p.sort()));
    
	const weights = _.sample(_.range(1, 100), st_pairs.length);

    return _.map(st_pairs, (stp,index) => {
	const [sid, tid] = stp;
	return {
	    group: "edges",
	    data: {
		weight:String(weights[index]),
		id: String(sid) + String(tid),
		source: String(sid),
		target: String(tid)
	    }
	};
    });
}

