import cytoscape from "cytoscape";
import dagre from "cytoscape-dagre";

import * as styles from "./styles.js";
import * as DG from "./dataGenerators.js";
import * as updates from "./updates.js";
import * as prompt from "./prompt.js";

import _ from "underscore";

cytoscape.use(dagre);

/* Message Box. */
const promptId = "prompt";
const promptMsg = "Select any node as root node to add it to the tree.";

prompt.update(promptId, promptMsg, prompt.alertType.Info);

const random_nodeids = DG.randomNodeIds();

var graph = cytoscape({
  container: document.getElementById("graph"),
  // autoungrabify: true,
  autounselectify: true,
  elements: {
    nodes: DG.randomNodes(random_nodeids),
    edges: DG.randomEdgesWithWeights(random_nodeids),
  },

  style: styles.graph_style_edge_labels,

  layout: {
    name: "cose",
  },
});

var tree_cy = cytoscape({
  container: document.getElementById("tree"),
  elements: {
    nodes: [],
    edges: [],
  },

  style: styles.tree_style_edge_labels,

  layout: {
    name: "dagre",
  },
});
tree_cy.layout({ name: "dagre" }).run();

graph.on("tap", "node", (e) => {
  let node = e.target;
  let id = node.id();

  if (node.classes().includes("selected")) {
    //removeLeaf(node, graph, tree_cy);
  } else {
    /*
	  Add the node to the tree.
	  find the edges that connects it to other nodes already in the tree.
	  decide which edge to use.
	 */

    /* Validation 
	   1. Either at least on neighbor should be selected already, or
	   2. This is the first node you pick.
	   3. Should not be candidate edge selection mode.
	*/

    const neighborSelectedNodes = graph
      .$("#" + node.id())
      .neighborhood("node")
      .filter((e) => {
        return e.hasClass("selected");
      });

    if (
      (!neighborSelectedNodes.empty() || tree_cy.nodes().empty()) &&
      !candidatesExist(graph)
    ) {
      node.classes("selected");
      highlightNeighbors(graph, node);
      tree_cy.add(node);
      tree_cy.center();
      showCandidates(graph, tree_cy, node.id());
    } else {
      // do nothing.  you cannot pick this node.
      console.log("Cannot pick this node");
      prompt.update(
        promptId,
        "Pick a minimum weighted candidate edge to add to the tree.",
        prompt.alertType.Info
      );
    }
  }
});

function highlightNeighbors(graph, node) {
  graph
    .$("#" + node.id())
    .neighborhood()
    .addClass("candidate");
}

function candidatesExist(g) {
  return !g
    .$((el) => {
      return el.hasClass("candidate");
    })
    .empty();
}

function bothNodesInTree(edge, tree) {
  /*
      given an edge, check if both source and target
      nodes are in the given tree (graph).
    */
  return !(
    tree.$("#" + edge.source().id()).empty() ||
    tree.$("#" + edge.target().id()).empty()
  );
}

function selectConnectingEdge(graph, tree, edge) {
  if (edge.hasClass("picked")) {
    /* Don't do anything if the edge is picked. */
    return;
  }

  if (bothNodesInTree(edge, tree)) {
    /* add this edge to the tree. */
    tree.add(edge);
    tree.center();

    /* styling */
    graph.edges().removeClass("candidate");
    edge.classes("picked");
  } else {
    //console.log("Cannot pick this edge.");
    prompt.update(promptId, "Cannot pick this edge", prompt.alertType.Info);
  }
}

function showCandidates(graph, tree, id) {
  graph
    .$("#" + id)
    .connectedEdges()
    .filter((edge) => {
      return !bothNodesInTree(edge, tree);
    })
    .classes("candidate");
  prompt.update(
    promptId,
    "Pick a minimum weighted candidate edge to add to the tree.",
    prompt.alertType.Info
  );
}

function selectNextStep(graph, tree, edge) {
  if (edge.hasClass("candidate")) {
    /* If the edge is a candidate, then select it and also select the connecting node
	   that has not been selected.
	   
	   And then remove all other candidates.
	*/
    if (!tree.nodes("#" + edge.source().id()).empty()) {
      if (tree.nodes("#" + edge.target().id()).empty()) {
        /* If the source already added to the tree and 
		   target is not, then we add the target node to the tree.
		*/
        tree.add(edge.target());

        /**/
        tree.add({
          data: {
            weight: edge.data("weight"),
            source: edge.source().id(),
            target: edge.target().id(),
          },
        });
        /**/

        edge.classes("picked");
        edge.target().addClass("selected");
        tree_cy.layout({ name: "dagre" }).run();
      } else {
        /* This condition should not be possible. */
        console.log(
          "A candidate edge cannot exist between two nodes that are already added to the tree"
        );
        prompt.update(
          promptId,
          "A candidate edge cannot exist between two nodes that are already added to the tree",
          prompt.alertType.Info
        );
      }
    } else {
      if (!tree.nodes("#" + edge.target().id()).empty()) {
        /* If the target already added to the tree and 
		   source is not, then we add the source node to the tree.
		*/
        tree.add(edge.source());
        /**/
        tree.add({
          data: {
            weight: edge.data("weight"),
            source: edge.target().id(),
            target: edge.source().id(),
          },
        });
        /**/

        edge.classes("picked");
        edge.source().addClass("selected");
        tree_cy.layout({ name: "dagre" }).run();
      } else {
        /* This condition should not be possible. */
        console.log(
          "A candidate edge cannot exist between two nodes that are not added to the tree"
        );
        prompt.update(
          promptId,
          "A candidate edge cannot exist between two nodes that are not added to the tree",
          prompt.alertType.Info
        );
      }
    }
    updateCandidates(graph, tree);
  }
}

function updateCandidates(g, t) {
  g.elements().removeClass("candidate");
  g.nodes((n) => n.hasClass("selected"))
    .closedNeighborhood((el) => {
      if (el.isEdge()) {
        return !(el.hasClass("picked") || bothNodesInTree(el, t));
      } else {
        return !el.hasClass("selected");
      }
    })
    //.forEach(el => console.log(el.id()))
    .addClass("candidate");
  prompt.update(
    promptId,
    "Pick a minimum weighted candidate edge to add to the tree.",
    prompt.alertType.Info
  );
}

function isSpanning(g, t) {
  /*
      Check if the tree is a spanning tree for the graph.
    */
  return g.nodes().difference(t.nodes()).empty();
}

graph.on("tap", "edge", (e) => {
  if (minWeightCandidate(e.target)) {
    graph
      .edges()
      .filter((e) => e.hasClass("incorrect"))
      .map((e) => e.removeClass("incorrect"));
    selectNextStep(graph, tree_cy, e.target);
    //  prompt.update(promptId, "Minimum Weighted Candidate edge Picked, Select Another Minimum Weighted Candidate Edge ");
  } else {
    e.target.addClass("incorrect");
    prompt.update(
      promptId,
      "Clicked Candidate edge does not have the Minimum Weight ",
      prompt.alertType.Danger
    );
    console.log(e.target.classes());
  }
  if (isSpanning(graph, tree_cy)) {
    prompt.update(
      promptId,
      "Minimum Spanning tree complete.",
      prompt.alertType.Success
    );
  }
});

function minWeightCandidate(edge) {
  const allCandidatesWeight = graph
    .edges()
    .filter((e) => e.hasClass("candidate"))
    .map((e) => e.data("weight"));
  const minCandidateWeight = Math.min(...allCandidatesWeight);
  if (edge.data("weight") <= minCandidateWeight) {
    return true;
  }
  return false;
}

// function makePopper(ele) {
//     let ref = ele.popperRef(); // used only for positioning

//     ele.tippy = tippy(ref, { // tippy options:
//       content: () => {
//         let content = document.createElement('div');

//         content.innerHTML = ele.id();

//         return content;
//       },
//       trigger: 'manual' // probably want manual mode
//     });
//   }

//   graph.ready(function() {
//     graph.elements().forEach(function(ele) {
//       makePopper(ele);
//     });
//   });
//   graph.elements().unbind('mouseover');
//   graph.elements().bind('mouseover', (event) => event.target.tippy.show());

//   graph.elements().unbind('mouseout');
//   graph.elements().bind('mouseout', (event) => event.target.tippy.hide());

// });
