import cytoscape from 'cytoscape';
import dagre from 'cytoscape-dagre';

import * as styles from "./styles.js";
import * as DG from "./dataGenerators.js";
// import * as updates from "./updates.js";
import * as prompt from "./prompt.js";

import _ from "underscore";

cytoscape.use( dagre );


/* Message Box. */
const promptId = "prompt";
const promptMsg = "Select any node to initialise DFS, and start the traversal.";
const btnId = "next";

const traversalList = [];
const stack = [];

document.getElementById(btnId).onclick = next;
document.getElementById(btnId).disabled = true;

prompt.update(promptId, promptMsg, prompt.alertType.Info);

const random_nodeids = DG.randomNodeIds();

var graph = cytoscape({
    container: document.getElementById('graph'),
    autoungrabify: true,
    autounselectify: true,
    elements: {
	nodes: DG.randomNodes(random_nodeids),
	edges: DG.randomEdges(random_nodeids)
    },
    
    style: styles.graph_style,
    
    layout: {
	name: 'cose'
    }
});


var tree_cy = cytoscape({
    container: document.getElementById('tree'),
    elements: {
	nodes: [],
	edges: []
    },
    
    style: styles.tree_style,
    
    layout: {
	name: 'dagre'
    }
});
tree_cy.layout({name: 'dagre'}).run();


graph.on('tap', 'node', (e) => {

    
    let node = e.target;
    let id = node.id();
	if (traversalList.length == 0 && stack.length == 0)
	{
		appendTraversalNode(node.id());
		stack.push(node.id());
		node.classes('selected');
		tree_cy.add(node);
	    tree_cy.center();
		logState();
		showCandidates();
	}
	else {
	    // do nothing.  you cannot pick this node.
	    console.log("Cannot pick this node");
	    prompt.update(promptId, "Pick a candidate edge, to visit adjacent node, and to add to the tree.",prompt.alertType.Info);
	}
});


function candidatesExist(g) {
    return !(g.$((el) =>
		 { return el.hasClass("candidate"); }
		).empty()
	    );
}


function bothNodesInTree(edge, tree) {
    /*
      given an edge, check if both source and target
      nodes are in the given tree (graph).
    */
    return !((tree.$("#" + edge.source().id()).empty())
	     || (tree.$("#" + edge.target().id()).empty())
	   );
}




function showCandidates() {
	const id = stack[stack.length-1];
	graph.nodes().removeClass('active');
	graph.$("#"+id).addClass('active');
	graph.edges().removeClass('candidate');
    graph.$("#"+id)
	.connectedEdges()
	.filter((edge) => {
	    return !bothNodesInTree(edge, tree_cy);
	})
	.classes("candidate");
    prompt.update(promptId, "Pick a candidate edge, to visit adjacent node, and to add to the tree.",prompt.alertType.Info);
	if(!candidatesExist(graph) && stack.length!=0){
		prompt.update(promptId,"No Candidate edges exist, backtrack to previous node.",prompt.alertType.Info);
	}
	viewNext();
	renderStack();
}



function selectNextStep(graph, tree, edge) {
    if( edge.hasClass("candidate") ) {
	/* If the edge is a candidate, then select it and also select the connecting node
	   that has not been selected.
	   
	   And then remove all other candidates.
	*/
	if( !tree.nodes("#" + edge.source().id()).empty() ) {
	    if( tree.nodes("#" + edge.target().id()).empty() ) {
		/* If the source already added to the tree and 
		   target is not, then we add the target node to the tree.
		*/				
		tree.add(edge.target());
		
		/**/
		tree.add({
		    data: {
			source: edge.source().id(),
			target: edge.target().id()
		    }
		});
		/**/

		appendTraversalNode(edge.target().id());
		stack.push(edge.target().id());
		edge.classes("picked");
		edge.target().addClass("selected");
		tree_cy.layout({name: 'dagre'}).run();
	    }
	    else {
		/* This condition should not be possible. */
		console.log("A candidate edge cannot exist between two nodes that are already added to the tree");
		prompt.update(promptId, "A candidate edge cannot exist between two nodes that are already added to the tree",prompt.alertType.Info);
	    }
	}
	else {
	    if( !tree.nodes("#" + edge.target().id()).empty() ) {
		/* If the target already added to the tree and 
		   source is not, then we add the source node to the tree.
		*/
		tree.add(edge.source());
		/**/
		tree.add({
		    data: {
			source: edge.target().id(),
			target: edge.source().id()
		    }
		});
		/**/

		appendTraversalNode(edge.source().id());
		stack.push(edge.source().id());
		edge.classes("picked");
		edge.source().addClass("selected");
		tree_cy.layout({name: 'dagre'}).run();
	    }
	    else {
		/* This condition should not be possible. */
		console.log("A candidate edge cannot exist between two nodes that are not added to the tree");
		prompt.update(promptId, "A candidate edge cannot exist between two nodes that are not added to the tree",prompt.alertType.Info);
	    }
	}
	showCandidates();
    }
    else {
	if (edge.hasClass("picked")) {
	    prompt.update(promptId, "Edge already picked, select a new edge from candidate edges.",prompt.alertType.Danger);
	}
	else {
	    prompt.update(promptId, "Not a candidate edge.",prompt.alertType.Info);
	}
    }
}



function appendTraversalNode(v){

    traversalList.push(v);
    console.log(traversalList);
    renderTraversal();
}

function renderTraversal() {
    const tdiv = document.getElementById("traversal");
    tdiv.innerHTML = _.reduce(traversalList,
			      (html, v) => html + `<span class="px-2 mx-2 bg-blue-100">${v}</span>`,
			      ""
			     );
}
function renderStack() {
    const tdiv = document.getElementById("stack");
    tdiv.innerHTML = _.reduce(stack,
			      (html, v) => html + `<span class="px-2 mx-2 bg-blue-100">${v}</span>`,
			      ""
			     );
}


function viewNext(){
	const nextBtn = document.getElementById(btnId);
	if (stack.length==0 && (!candidatesExist(graph) || traversalList.length==0))
	{
		nextBtn.disabled=true;
	}
	else
	{
	nextBtn.disabled=false;
	}
}


function backtrack(){
	stack.pop();
	showCandidates();
	if( isSpanning(graph, tree_cy) ){
		prompt.update(promptId, "Depth First Traversal finished, spanning tree completed.",prompt.alertType.Success);
	}
	logState();
}



function isSpanning(g, t) {
    /*
      Check if the tree is a spanning tree for the graph.
    */
   return g.nodes().difference(t.nodes()).empty() && (stack.length==0) ;
}





function next(){
	if (traversalList.length == 0 && stack.length == 0)
	{
		/* Do Nothing */
	}
	else {
	    if(candidatesExist(graph)){
			const edge = _.sample(graph.edges().filter(e=>e.hasClass('candidate')));
			selectNextStep(graph, tree_cy, edge);
			if( isSpanning(graph, tree_cy) ){
			prompt.update(promptId, "Depth First Traversal finished, spanning tree completed.",prompt.alertType.Success);
			}
		}
		else if (stack.length!=0){
			backtrack();
		}
	}
}

function logState() {
	console.log("Visited:",traversalList,"\n Stack:",stack);
}