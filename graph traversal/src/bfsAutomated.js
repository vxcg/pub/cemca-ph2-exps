import cytoscape from 'cytoscape';
import dagre from 'cytoscape-dagre';

import * as styles from "./styles.js";
import * as DG from "./dataGenerators.js";
// import * as updates from "./updates.js";
import * as prompt from "./prompt.js";

import _ from "underscore";

cytoscape.use( dagre );


/* Message Box. */
const promptId = "prompt";
const promptMsg = "Select any node to initialise BFS, and start the traversal.";
const btnId = "next";
document.getElementById(btnId).disabled=true;
document.getElementById(btnId).onclick = next;



const traversalList = [];
const queue = [];
const parents = [];


prompt.update(promptId, promptMsg,prompt.alertType.Info);

const random_nodeids = DG.randomNodeIds();

var graph = cytoscape({
    container: document.getElementById('graph'),
    autoungrabify: true,
    autounselectify: true,
    elements: {
	nodes: DG.randomNodes(random_nodeids),
	edges: DG.randomEdges(random_nodeids)
    },
    
    style: styles.graph_style,
    
    layout: {
	name: 'cose'
    }
});


var tree_cy = cytoscape({
    container: document.getElementById('tree'),
    elements: {
	nodes: [],
	edges: []
    },
    
    style: styles.tree_style,
    
    layout: {
	name: 'dagre'
    }
});
tree_cy.layout({name: 'dagre'}).run();


graph.on('tap', 'node', (e) => {

    
    let node = e.target;
    let id = node.id();
	if (traversalList.length == 0 && queue.length == 0)
	{
		appendTraversalNode(node.id());
		node.classes('selected');
		tree_cy.add(node);
	    tree_cy.center();
		Enqueue(node)
		logState();
		showCandidates();
	}
	else {
	    // do nothing.  you cannot pick this node.
	    console.log("Cannot pick this node");
	    prompt.update(promptId, "Pick a candidate edge, to visit the adjacent node, and add to the tree.",prompt.alertType.Info);
	}
});


function candidatesExist(g) {
    return !(g.$((el) =>
		 { return el.hasClass("candidate"); }
		).empty()
	    );
}


function bothNodesInTree(edge, tree) {
    /*
      given an edge, check if both source and target
      nodes are in the given tree (graph).
    */
    return !((tree.$("#" + edge.source().id()).empty())
	     || (tree.$("#" + edge.target().id()).empty())
	   );
}




function showCandidates() {
	const id = queue[queue.length-1];
	graph.edges().removeClass('candidate');
    if(queue[0]!=undefined){
		const nodeIds = queue[0].map(n=> "#" + n).join(", "); 
		const parentNode = graph.$("#"+parents[0]);
		graph.$(nodeIds).edgesWith(parentNode).addClass('candidate');
	}
    prompt.update(promptId, "Pick a candidate edge, to visit the adjacent node, and add to the tree.",prompt.alertType.Info);
	renderQueue();
	showDiscovered();
	viewNext();
	logState();
}

function showDiscovered(){
	graph.nodes().filter(e=>e.hasClass('discovered')).removeClass('discovered');
	if(queue.length!=0){
		const nodeIds = queue.flat().map(n=> "#" + n).join(", "); 
		graph.$(nodeIds).addClass('discovered');
	}
}


function selectNextStep(graph, tree, edge) {
    if( edge.hasClass("candidate") ) {
	/* If the edge is a candidate, then select it and also select the connecting node
	   that has not been selected.
	   
	   And then remove all other candidates.
	*/
	if( !tree.nodes("#" + edge.source().id()).empty() ) {
	    if( tree.nodes("#" + edge.target().id()).empty() ) {
		/* If the source already added to the tree and 
		   target is not, then we add the target node to the tree.
		*/				
		tree.add(edge.target());
		
		/**/
		tree.add({
		    data: {
			source: edge.source().id(),
			target: edge.target().id()
		    }
		});
		/**/

		appendTraversalNode(edge.target().id());
		Dequeue(edge);
		Enqueue(edge.target());
		edge.classes("picked");
		edge.target().addClass("selected");
		tree_cy.layout({name: 'dagre'}).run();
	    }
	    else {
		/* This condition should not be possible. */
		console.log("A candidate edge cannot exist between two nodes that are already added to the tree");
		prompt.update(promptId, "A candidate edge cannot exist between two nodes that are already added to the tree",prompt.alertType.Info);
	    }
	}
	else {
	    if( !tree.nodes("#" + edge.target().id()).empty() ) {
		/* If the target already added to the tree and 
		   source is not, then we add the source node to the tree.
		*/
		tree.add(edge.source());
		/**/
		tree.add({
		    data: {
			source: edge.target().id(),
			target: edge.source().id()
		    }
		});
		/**/

		appendTraversalNode(edge.source().id());
		Dequeue(edge);
		Enqueue(edge.source());
		edge.classes("picked");
		edge.source().addClass("selected");
		tree_cy.layout({name: 'dagre'}).run();
	    }
	    else {
		/* This condition should not be possible. */
		console.log("A candidate edge cannot exist between two nodes that are not added to the tree");
		prompt.update(promptId, "A candidate edge cannot exist between two nodes that are not added to the tree",prompt.alertType.Info);
	    }
	}
	showCandidates();
    }
    else {
	if (edge.hasClass("picked")) {
	    prompt.update(promptId, "Edge already picked, select a new edge from candidate edges.",prompt.alertType.Danger);
	}
	else {
	    prompt.update(promptId, "Not a candidate edge.",prompt.alertType.Info);
	}
    }
}



function appendTraversalNode(v){

    traversalList.push(v);
    renderTraversal();
}

function renderTraversal() {
    const tdiv = document.getElementById("traversal");
    tdiv.innerHTML = _.reduce(traversalList,
			      (html, v) => html + `<span class="px-2 mx-2 bg-blue-100">${v}</span>`,
			      ""
			     );
}

function leadsToUnVisitedNodeUndiscoveredNode(e,nodeId){
	const sId = e.source().id();
	const tId = e.target().id();
	if(sId == nodeId){
		if(traversalList.includes(tId))
			return false;
		if(queue.flat().includes(tId))
			return false;
	}
	else{
		if(traversalList.includes(sId))
			return false;
		if(queue.flat().includes(sId))
			return false;
	}
	return true;
}



function Enqueue(node){
	const id = node.id();
	const candidateEdges = graph.$("#"+id).connectedEdges().filter((edge) => leadsToUnVisitedNodeUndiscoveredNode(edge,id));
	const undiscoveredNodes = candidateEdges.map(e=>getUnvistedNode(e,id));
	if(undiscoveredNodes.length!=0){
		queue.push(undiscoveredNodes);
		parents.push(id);
	}
}

function Dequeue(edge){
	const sId = edge.source().id();
	const tId = edge.target().id();
	if(queue[0]!=undefined){
		const id = sId == parents[0]? tId : sId;
		const index = queue[0].indexOf(id);
		if (index > -1) {
			queue[0].splice(index, 1);
			if(queue[0].length==0){
				queue.splice(0,1);
				parents.splice(0,1);
			}
		}
	}
}

function getUnvistedNode(edge,nodeId){
	
	if(edge.source().id()==nodeId){
		return edge.target().id();
	}
	else{
		return edge.source().id();
	}
}


function renderQueue() {
    const tdiv = document.getElementById("queue");
	const newQueue = queue.map(e=>"{ "+e.join(", ")+" }");
    tdiv.innerHTML = _.reduce(newQueue,
			      (html, v) => html + `<span class="px-2 mx-2 bg-blue-100">${v}</span>`,
			      ""
			     );
}


function viewNext(){
	const nextBtn = document.getElementById(btnId);
	if (queue.length==0 && (!candidatesExist(graph) || traversalList.length==0))
	{
		nextBtn.disabled=true;
	}
	else
	{
	nextBtn.disabled=false;
	}
}


function next(){
	if (traversalList.length == 0 && stack.length == 0)
	{
		/* Do Nothing */
	}
	else {
	    if(candidatesExist(graph)){
			const edge = _.sample(graph.edges().filter(e=>e.hasClass('candidate')));
			selectNextStep(graph, tree_cy, edge);
			if( isSpanning(graph, tree_cy) ){
			prompt.update(promptId, "Breadth First Traversal finished, spanning tree completed.",prompt.alertType.Success);
			}
		}
	}
}





function isSpanning(g, t) {
    /*
      Check if the tree is a spanning tree for the graph.
    */
   return g.nodes().difference(t.nodes()).empty() && (queue.length==0) ;
}



function logState() {
	console.log("Visited:",traversalList,"\n Queue:",queue,"\n Parents :",parents);
}