const path = require("path");

module.exports = {
  entry: {
    "js/spanningTree": "./src/spanningTree.js",
    "js/connectedSpanningTree": "./src/connectedSpanningTree.js",
    "js/dfsAlgo": "./src/dfsAlgo.js",
    "js/dfsAutomated": "./src/dfsAutomated.js",
    "js/bfsAlgo": "./src/bfsAlgo.js",
    "js/bfsAutomated": "./src/bfsAutomated.js",
    "js/nav": "./src/nav.js",
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "build"),
  },
};
