const path = require("path");

module.exports = {
  entry: {
    "js/mst": "./src/mst.js",
    "js/primsAlgo": "./src/primsAlgo.js",
    "js/kruskalsAlgo": "./src/kruskalsAlgo.js",
    "js/nav": "./src/nav.js",
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "build"),
  },
};
