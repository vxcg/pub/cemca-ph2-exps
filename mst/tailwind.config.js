module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        "alert-green": {
          text: '#155724',
          bg: '#d4edda',
          border: '#c3e6cb'
        },
        "alert-blue": {
          text: "#004085",
          bg: "#cce5ff",
          border: "#b8daff"
        },
        "alert-red": {
          text: "#721c24",
          bg: "#f8d7da",
          border: "#f5c6cb"
        }
      }
    },
  },
  variants: {
    opacity: ({ after }) => after(['disabled'])
  },
  plugins: [],
}
