const dagre = require('dagre');

window.onload = () => {

    var app = Elm.DFS.init({
	node: document.getElementById('dfs')
    });

    app.ports.sendMessage.subscribe(function(graphData) {

	g = dagre.graphlib.json.read(graphData);
	g.setGraph({nodesep: 200});
	dagre.layout(g);
	
	app.ports
	    .messageReceiver
	    .send(JSON.stringify(dagre.graphlib.json.write(g)));
    });

}
